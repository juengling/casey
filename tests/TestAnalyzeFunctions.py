'''
Test module for AnalyzeFunctions
'''
import unittest

from AnalyzeFunctions import get_declarations_from_content, get_declaration_patterns


class TestAnalyzeFunctions(unittest.TestCase):
    '''
    Unit tests for module "AnalyzeFunctions"
    '''

    def setUp(self):
        '''
        Prepare anything that is necessary for any tests in this class.
        '''
        self.declaration_commands = get_declaration_patterns()

    def test_detect_declarations(self):
        '''
        Declarations shall be found.
        '''
        filecontent = '''
Public Sub DontKnowWhy()

Dim x As Integer
Dim s As String

End Sub
'''
        declarations = get_declarations_from_content(filecontent, self.declaration_commands)

        self.assertEqual(len(declarations), 3, 'Wrong number of declarations')
        self.assertTrue('DontKnowWhy' in declarations)
        self.assertTrue('x' in declarations)
        self.assertTrue('s' in declarations)

    def test_detect_no_declarations_inside_comment(self):
        '''
        Declarations inside a comment must not be found.
        '''
        filecontent = '''
Public Sub DontKnowWhy()

Const FUNCTION_NAME="DontKnowWhy"

'Dim ignored As Boolean ' Not in use anymore

End Sub
'''
        declarations = get_declarations_from_content(filecontent, self.declaration_commands)

        self.assertEqual(len(declarations), 2, 'Wrong number of declarations')
        self.assertTrue('DontKnowWhy' in declarations)
        self.assertFalse('ignored' in declarations)

    def test_detect_no_declarations_inside_string(self):
        '''
        Declarations inside a literal string must not be found.
        '''
        filecontent = '''
Public Sub DontKnowWhy()

Dim s As String

s = "Dim Word As String"

End Sub
'''
        declarations = get_declarations_from_content(filecontent, self.declaration_commands)

        self.assertEqual(len(declarations), 2, 'Wrong number of declarations')
        self.assertTrue('DontKnowWhy' in declarations, 'DontKnowWhy not found')
        self.assertTrue('s' in declarations, 's not found')
        self.assertFalse(
            'Word' in declarations, 'Word was found, but should not!')

    def test_detect_no_declarations_inside_literalstring(self):
        '''
        Declarations inside a comment must not be found.
        '''
        filecontent = '''
Public Sub DontKnowWhy()

Const FUNCTION_NAME = "DontKnowWhy"

Dim x As Integer
Dim s As String
'Dim ignored As Boolean ' Not in use anymore

End Sub
'''
        declarations = get_declarations_from_content(filecontent, self.declaration_commands)

        self.assertEqual(len(declarations), 4, 'Wrong number of declarations')
        self.assertTrue('DontKnowWhy' in declarations, 'DontKnowWhy not found')
        self.assertTrue(
            'FUNCTION_NAME' in declarations, 'FUNCTION_NAME not found')
        self.assertTrue('x' in declarations, 'x not found')
        self.assertTrue('s' in declarations, 's not found')
        self.assertFalse(
            'ignored' in declarations, 'ignored was found, but should not!')


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
