Download and Installation
=========================

Download
--------

During the test the latest version can be obtained from

    https://gitlab.com/juengling/casey/pipelines

Every built version here lives for 4 weeks. After the test ist over,
final releases shall be found at

	https://gitlab.com/juengling/casey/tags


Installation
------------

Just run the setup (english or german) and read carefully any settings
and tasks you may choose. That aren't that much.

See chapter :ref:`usage` for more information.
