'''
Created on 26.03.2016

@author: chris
'''
from glob import glob
from os import path
from os.path import expanduser
from pprint import pformat, pprint
import codecs
import re

from tqdm._tqdm import tqdm

import public


def get_default_file_patterns(project_type):
    '''
    Return a list of patterns for file search and where any action inside the file shall begin.
    Previous lines of that file are ignored. If 'startat' is an empty string, file actions
    start immediately at the beginning.

    Each dictionary in the list consists of two elements:
        * 'pattern': The file matching pattern to collect the files to be scanned
        * 'startat': The regex, which indicates where the search has to begin (i.e. on the following line)

    :param str project_type: Type of the project ('access' or 'vb6')
    :return: File patterns with startat information
    :rtype: List of dicts
    '''

    if project_type.lower() == 'access':
        # Access with OASIS
        result = [
                  {'pattern': 'm_*.def', 'startat': ''},
                  {'pattern': 'f_*.def', 'startat': '^CodeBehindForm$'},
                  {'pattern': 'r_*.def', 'startat': '^CodeBehindForm$'}
                  ]
    elif project_type.lower() == 'vb6':
        # VB6
        result = [
                  {'pattern': '*.bas', 'startat': ''},
                  {'pattern': '*.cls', 'startat': ''},
                  {'pattern': '*.frm', 'startat': 'Attribute VB_Name'},
                  {'pattern': '*.Dsr', 'startat': 'Attribute VB_Name'}
                  ]
    else:
        raise Exception('Unknown project type: %s', project_type)

    if public.verbose >= 2:
        print('{} file patterns set'.format(len(result)))

    return result


def get_declaration_patterns():
    '''
    Return a list of all declaration command patterns (regular expressions)
    for the Visual Basic language

    :return: Declaration patterns
    :rtype: List
    '''

    # TODO: Read this information from an optional configuration file

    result = [
        r'^Private WithEvents (\w+)',
        r'^Public WithEvents (\w+)',
        r'^Const (\w+)',
        r'^Private Const (\w+)',
        r'^Public Const (\w+)',
        r'^Private Type (\w+)',
        r'^Public Type (\w+)',
        r'^Private Enum (\w+)',
        r'^Public Enum (\w+)',
        r'^Private Sub (\w+)\(',
        r'^Public Sub (\w+)\(',
        r'^Private Function (\w+)\(',
        r'^Public Function (\w+)\(',
        r'^Private Property (\w+)\(',
        r'^Public Property (\w+)\(',
        r'^Dim (\w+) As',
        r'^Private (\w+) As',
        r'^Public (\w+) As',
    ]

    if public.verbose >= 2:
        print('{} declaration patterns set'.format(len(result)))

    return result


def get_declarations_from_content(content, declaration_patterns, start_at=''):
    '''
    Walk through the content and collect all declarations from declaration_commands list
    with their correct spelling, starting at start_at.

    :param str content: Complete content of any file
    :param list declaration_patterns: Declaration patterns
    :param str start_at: Regex to find first line for changing
    :return: Declarations found in the content
    :rtype: list
    '''

    declarations = []
    start = len(start_at) == 0
    for cont in content.split('\n'):
        cont = cont.strip()
        if start:
            for decl in declaration_patterns:
                match = re.match(decl, cont)
                if match and len(match.groups()) > 0:
                    declarations.append(match.group(1))
        else:
            if re.match(start_at, cont):
                start = True

    return declarations


def collect_all_files(basefolder):
    '''
    Collect all files that match any file pattern

    :param basefolder: Folder to search in
    :type basefolder: String
    :return: Matching files
    :rtype: List
    '''
    file_list = []
    file_patterns = get_default_file_patterns()

    for pattern in file_patterns:
        filepattern = path.join(basefolder, pattern['pattern'])
        file_list += get_file_list(filepattern, pattern['startat'])

    return file_list


def collect_declarations(filelist):
    '''
    Collect any declaration from the source code, store them in a list and return the list

    :param filelist: Filenames/paths to collect from
    :type filelist: List
    :return: Declarations of any file
    :rtype: List
    '''
    declaration_patterns = get_declaration_patterns()
    declarations = set()
    for filename in tqdm(filelist, desc='Analyse files'):
        with codecs.open(filename, 'r', encoding=get_codec()) as file:
            decl = get_declarations_from_content(file.read(), declaration_patterns, filelist[filename])

        if public.verbose >= 3:
            print('{} declarations found in {}'.format(
                len(decl), filename))

        for x in decl:
            declarations.add(x)

    if public.verbose >= 3:
        pprint(sorted(declarations))

    if public.verbose >= 2:
        print('{} declarations found'.format(len(declarations)))

    return declarations


def get_codec():
    '''
    Return codec used to open the files

    :return: Codec string
    :rtype: String
    '''
    return 'iso-8859-15'


def get_file_list(filepattern, startat):
    '''
    Retrieve files, starting in the given folder.

    :param str filepattern: Fully qualified folder with file pattern
    :param str startat: Regex to identify first line to start replacement
    :return: Filenames with path
    :rtype: list
    '''

    files = {}

    # Retrieve files in this folder
    for filename in glob(expanduser(filepattern)):
        files[filename] = startat

    return files
